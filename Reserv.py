import subprocess
import time
import re
from datetime import datetime
from flask import Flask, render_template_string

app = Flask(__name__)

# Configuration des horaires
HOURS_EARLY_LATE = [(0,6), (11,23)]
HOURS_ALLOWED = [(7, 10), (10, 11)]

# Configuration des messages
MESSAGES = {
	"early_late": "La réservation n'est pas ouverte. Vous pouvez signaler votre présence au restaurant entre 8H et 10H.",
	"already_registered": "Votre présence est déjà enregistrée.",
	"registered": "Merci, votre présence est enregistrée.",
	"last_minute": "Oulah c'est juste mais nous transmettons votre présence."
}

# Fonction pour vérifier si l'heure actuelle est dans la plage horaire
def is_time_in_range(start, end, current):
	if start <= end:
		return start <= current <= end
	else:
		return start <= current pr current <= end

# Fonction pour afficher le message popup
# @app.route('/popup/<message>')
#def popup(message):
	#return render_template_string(html)

# Initialiser la liste des badges
liste_badges = []

# Fonction pour vérifier si une carte est déjà enregistrée
def check_card(card_id):
    return any(entry['card_id'] == card_id for entry in liste_badges)

# Fonction pour enregistrer une carte
def register_card(card_id):
	current_time = datetime.now().strftime('%H:%M:%S %d/%m/%Y')
	liste_badges.append({"card_id": card_id, "datetime": current_time})

# Durée pendant laquelle le programme doit tourner (en secondes)
duree_execution = 15
temps_debut = time.time()

while time.time() - temps_debut < duree_execution:
    try:
        # Appeler le programme nfctools.py avec la commande getuid
        result = subprocess.run(['python3', 'nfctool.py', 'getuid'], capture_output=True, text=True)
        
        # Vérifier si la commande a réussi
        if result.returncode == 0:
            # Extraire l'UID du retour de la commande en utilisant une expression régulière
            output = result.stdout
            match = re.search(r'getuid: ([0-9A-F ]+)', output)
            if match:
                uid = match.group(1).strip()
                current_hour = datetime.now().hour

                if is_time_in_range(0, 6, current_hour) or is_time_in_range(11, 23, current_hour):
                    app.run(debug=True, use_reloader=False)  # Lancez le serveur Flask pour afficher le message
                    print(MESSAGES["early_late"])
                elif is_time_in_range(7, 10, current_hour):
                    if check_card(uid):
                        app.run(debug=True, use_reloader=False)
                        print(MESSAGES["already_registered"])
                    else:
                        register_card(uid)
                        app.run(debug=True, use_reloader=False)
                        print(MESSAGES["registered"])
                elif is_time_in_range(10, 11, current_hour):
                    if check_card(uid):
                        app.run(debug=True, use_reloader=False)
                        print(MESSAGES["already_registered"])
                    else:
                        register_card(uid)
                        app.run(debug=True, use_reloader=False)
                        print(MESSAGES["last_minute"])

            else:
                print("UID non trouvé dans la sortie de la commande.")
        else:
            print(f"Erreur dans l'exécution de la commande: {result.stderr}")
        
        # Pause avant de relancer la commande
        time.sleep(1)
    
    except Exception as e:
        print(f"Erreur: {e}")

# Imprimer la liste des badges et leur nombre
print("Liste des badges:")
print(liste_badges)
print(f"Nombre de badges: {len(liste_badges)}")
